﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	public GameObject Bullet_Emitter;
	public GameObject Bullet;

	private Color color = Color.magenta;
	private LineRenderer lineRenderer;
	private float angle = 0.0f;
	private float force = 3.0f;

	void Start () {
		lineRenderer = gameObject.AddComponent<LineRenderer>();
		lineRenderer.material = new Material(Shader.Find("VertexLit"));
		lineRenderer.SetWidth(0.2f,0.2f);
		lineRenderer.SetVertexCount(6);
    }

	void Update () {
		if (angle < 70.0f && Input.GetKey("up")) {
			angle += 1.0f;
		} else if (angle > 0.0f && Input.GetKey("down")) {
			angle -= 1.0f;
		}

		Vector3 startPoint = Bullet_Emitter.transform.position;
		Vector3 endPoint = startPoint + Vector3.up * force * Mathf.Sin(angle * Mathf.PI / 180.0f) 
			+ Bullet_Emitter.transform.forward * force * Mathf.Cos(angle * Mathf.PI / 180.0f);
		Vector3 tmpLine = (endPoint - startPoint) * 0.1f;
		Vector3 leftArrowPoint = endPoint - Bullet_Emitter.transform.right * 0.5f - tmpLine ;
		Vector3 rightArrowPoint = endPoint + Bullet_Emitter.transform.right * 0.5f - tmpLine;
	
		lineRenderer.SetPosition(0, startPoint);
		lineRenderer.SetPosition(1, endPoint);
		lineRenderer.SetPosition(2, leftArrowPoint);
		lineRenderer.SetPosition(3, endPoint);
		lineRenderer.SetPosition(4, rightArrowPoint);
		lineRenderer.SetPosition(5, endPoint);

		if (Input.GetKey ("space")) {
			force = (force + 0.3f) % 60.0f;

		}

        if (Input.GetKeyUp("space")) {
			GameObject Temporary_Bullet_Handler;
			Temporary_Bullet_Handler = Instantiate(Bullet,Bullet_Emitter.transform.position,Bullet_Emitter.transform.rotation) as GameObject;

			Rigidbody Temporary_RigidBody;
			Temporary_RigidBody = Temporary_Bullet_Handler.GetComponent<Rigidbody>();


			float realForce = force > 20.0f ? force * 15.0f : force * 8.0f;
			force = 3.0f;
			Vector3 forceVector = Bullet_Emitter.transform.forward * realForce + Vector3.up * realForce * Mathf.Tan (angle * Mathf.PI / 180.0f);
			Temporary_RigidBody.AddForce(forceVector, ForceMode.Impulse);
            Temporary_Bullet_Handler.tag = "Ball";

            Destroy(Temporary_Bullet_Handler, 5.0f);
        }
    }
}
