﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootTarget : MonoBehaviour
{
    public GameObjectManager gameObjectManager;
    private bool target;

    public void SetRed()
    {
        GetComponent<Renderer>().material.color = Color.red;
        target = true;
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Ball" && target)
        {
            gameObjectManager.ReactOnTargetHit(this);
            GetComponent<Renderer>().material.color = Color.white;
            target = false;
        }
    }

}
