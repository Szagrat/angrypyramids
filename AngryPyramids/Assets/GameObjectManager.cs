﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameObjectManager : MonoBehaviour {
    public List<ShootTarget> shootTargets;
	public Text uiText;
    public Text uiTextLastHit;
    public Text uiTextScore;
    public Text uiTextWin;
    private bool notChoosenTarget;
    private List<int> oldTargets;
    private int targetHited;
    private float lastHitTime;
    private float diffInTime;
    private float sumTime;
    private int score;
    // Use this for initialization
    void Start () {
        score = 0;
        lastHitTime = Time.time;
        targetHited = 0;
        oldTargets = new List<int>();
        notChoosenTarget = false;
        ChangeTarget();
        uiTextWin.text = "";
    }

    // Update is called once per frame
    void Update () {
		uiText.text = "TARGETS TO HIT: " + (shootTargets.Count - targetHited).ToString();
        uiTextLastHit.text = "LAST HIT: " + string.Format("{0:0.00}", diffInTime);
        uiTextScore.text = "SCORE: " + score.ToString();
    }

    public void ReactOnTargetHit(ShootTarget tg)
    {
        ChangeTarget();
    }

    public void ChangeTarget()
    {
        if (targetHited < shootTargets.Count)
        {
            int randomNumber = Random.Range(0, shootTargets.Count);
            while (oldTargets.Contains(randomNumber)) randomNumber=Random.Range(0, shootTargets.Count);
            oldTargets.Add(randomNumber);
            var choosen = shootTargets[randomNumber];
            choosen.SetRed();
            notChoosenTarget = true;
            
            diffInTime = Time.time - lastHitTime;
            lastHitTime = Time.time;
            targetHited++;
            if (diffInTime != 0) score += (int) (100/diffInTime);
        }
        else
        {
            sumTime = Time.time;
            uiTextWin.text = "You win! Your score " + score.ToString() + " after time " + sumTime.ToString();
        }
    }

}
