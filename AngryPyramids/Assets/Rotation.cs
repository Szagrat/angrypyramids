﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour {
    private int RotationSpeed;
	// Use this for initialization
	void Start () {
        RotationSpeed = 100;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.up * (RotationSpeed * Time.deltaTime));

    }
}
